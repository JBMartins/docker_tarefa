FROM alpine

LABEL maintainer="Joao Martins"

COPY equipa1/ /opt/equipa1

WORKDIR /opt/equipa1

RUN ls

CMD ["cat","dados.xml"]
